package com.example.demobeans.beans;

/**
 * This is a marker class used to tell Spring to scan the package this class 
 * is a member of.
 */
public class HibernateConfigurationMarker {

}
