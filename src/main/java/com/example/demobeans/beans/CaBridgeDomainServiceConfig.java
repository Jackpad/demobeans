package com.example.demobeans.beans;

import com.example.demobeans.CertificateProductApplicationService;
import com.example.demobeans.CertificateProductApplicationServiceCabImpl;
import com.example.demobeans.CertificateProgramApplicationService;
import com.example.demobeans.CertificateProgramApplicationServiceCabImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
/*
 * We must tell spring to scan for @Bean classes by package if the classes are
 * not in the same package as this class.  Each class given is a marker class which
 * tells spring to scan that class's package.
 */
@ComponentScan(basePackageClasses = {HibernateConfigurationMarker.class})
public class CaBridgeDomainServiceConfig {

	public static final String CERTIFICATE_PRODUCT_APP_SERVICE = "certificateProductAppService";
	public static final String CERTIFICATE_PROGRAM_APP_SERVICE = "certificateProgramAppService";

	@Bean(name = CERTIFICATE_PRODUCT_APP_SERVICE)
	public CertificateProductApplicationService certificateProductAppService() {
		return new CertificateProductApplicationServiceCabImpl();
	}

	@Bean(name = CERTIFICATE_PROGRAM_APP_SERVICE)
	public CertificateProgramApplicationService certificateProgramAppService() {
		return new CertificateProgramApplicationServiceCabImpl();
	}


}
