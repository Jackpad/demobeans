package com.example.demobeans.beans;

import java.util.Arrays;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/*
 * Tell Spring where to scan for @Configuration, etc classes
 */
@EnableAutoConfiguration
@ComponentScan(basePackageClasses = {
		CaBridgeDomainServiceConfig.class,
		HibernateConfigurationMarker.class})
@Slf4j
public class ServerNexusMain {

	public static void main(String[] args) {
		ServerNexusMain instance = new ServerNexusMain();
		instance.Main(args);
	}

	public void Main(String[] args) {
		reportSpring();
		DomainRegistryCab.certificateProductAppService();
		DomainRegistryCab.certificateProgramAppService();
	}

	private void reportSpring() {
		String[] beanNames = DomainRegistryCab.applicationContext().getBeanDefinitionNames();
		if (beanNames != null) {
			log.info("Spring bean names: \n{}", Arrays.stream(beanNames).collect(Collectors.joining("\n ")));
		}
	}
}
