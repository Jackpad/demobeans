package com.example.demobeans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = {HibernateConfigurationMarker.class})
public class CaBridgeDomainServiceConfig {

	public static final String CERTIFICATE_PRODUCT_APP_SERVICE = "certificateProductAppService";
	public static final String CERTIFICATE_PROGRAM_APP_SERVICE = "certificateProgramAppService";

	@Bean(name = CERTIFICATE_PRODUCT_APP_SERVICE)
	public CertificateProductApplicationService certificateProductAppService() {
		return new CertificateProductApplicationServiceCabImpl();
	}

	@Bean(name = CERTIFICATE_PROGRAM_APP_SERVICE)
	public CertificateProgramApplicationService certificateProgramAppService() {
		return new CertificateProgramApplicationServiceCabImpl();
	}
}

