package com.example.demobeans;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.Assert;

public class DomainRegistryCab {

	private static ApplicationContext applicationContext;

	private static ApplicationContext createApplicationContext() {
		return new AnnotationConfigApplicationContext(CaBridgeDomainServiceConfig.class);
	}

	public static CertificateProductApplicationService certificateProductAppService() {
		var service = applicationContext().getBean(CertificateProductApplicationServiceCabImpl.class);
		validateDataSourceIs(DataStore.ProductDataStore, service.dataSource());
		return service;
	}

	private static void validateDataSourceIs(DataStore ds1, DataStore ds2) {
		Assert.isTrue(ds1 == ds2, "error");
	}

	public static CertificateProgramApplicationService certificateProgramAppService() {
		var service = applicationContext().getBean(CertificateProgramApplicationServiceCabImpl.class);
		validateDataSourceIs(DataStore.ProgramDataStore, service.dataSource());
		return service;
	}

	public static void main(String[] args) {
		CertificateProductApplicationService certificateProductApplicationService = certificateProductAppService();
		CertificateProgramApplicationService certificateProgramApplicationService = certificateProgramAppService();
		System.out.println("xx");
	}

	public static ApplicationContext applicationContext() {
		if (applicationContext == null) {
			applicationContext = createApplicationContext();
		}
		return applicationContext;
	}
}