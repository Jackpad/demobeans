package com.example.demobeans.beans;

import com.example.demobeans.CertificateProductApplicationService;
import com.example.demobeans.CertificateProductApplicationServiceCabImpl;
import com.example.demobeans.CertificateProgramApplicationService;
import com.example.demobeans.CertificateProgramApplicationServiceCabImpl;
import com.example.demobeans.DataStore;
import javax.sql.DataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.util.Assert;


public class DomainRegistryCab {

	private static ApplicationContext applicationContext;

	private static ApplicationContext createApplicationContext() {
		/*
		 * Specify all @Configuration classes we want to scan
		 */
		return new AnnotationConfigApplicationContext(CaBridgeDomainServiceConfig.class);
	}

	public static CertificateProductApplicationService certificateProductAppService() {
		// CONFIG1
//    	var service = BeanFactoryAnnotationUtils.qualifiedBeanOfType(
//    			applicationContext().getAutowireCapableBeanFactory(),
//    			CertificateProductApplicationService.class,
//    			CaBridgeDomainServiceConfig.CERTIFICATE_PRODUCT_APP_SERVICE);

		// CONFIG2
		// RESULT:  org.springframework.beans.factory.BeanNotOfRequiredTypeException: Bean named 'certificateProductAppServiceImpl' is expected to be of type 'cmb.cabridge.application.cert.CertificateProductApplicationServiceCabImpl' but was actually of type 'com.sun.proxy.$Proxy62'
//    	var service = applicationContext().getBean(
//			CaBridgeDomainServiceConfig.CERTIFICATE_PRODUCT_APP_SERVICE_IMPL,
//			CertificateProductApplicationServiceCabImpl.class);

		// CONFIG3
		// RESULT: Returns CertificateProgramApplicationServiceCabImpl
//    	var service = applicationContext().getBean(
//			CaBridgeDomainServiceConfig.CERTIFICATE_PRODUCT_APP_SERVICE,
//			CertificateProductApplicationService.class);

		// CONFIG4
		// RESULT:  org.springframework.beans.factory.NoUniqueBeanDefinitionException: No qualifying bean of type 'cmb.domain.model.cert.CertificateProductApplicationService' available: expected single matching bean but found 2: certificateProductAppService,certificateProductAppServiceImpl
//    	var service = applicationContext().getBean(CertificateProductApplicationService.class);

		// CONFIG5
		// RESULT:  org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 'cmb.cabridge.application.cert.CertificateProductApplicationServiceCabImpl' available
		var service = applicationContext().getBean(CertificateProductApplicationServiceCabImpl.class);

		// CONFIG6
		// RESULT: Returns CertificateProgramApplicationServiceCabImpl
//    	CertificateProductApplicationService service = (CertificateProductApplicationService) 
//    			applicationContext().getBean(CaBridgeDomainServiceConfig.CERTIFICATE_PRODUCT_APP_SERVICE);

		validateDataSourceIs(DataStore.ProductDataStore, service.dataSource());
		return service;
	}

	public static CertificateProgramApplicationService certificateProgramAppService() {
		// CONFIG1
//    	var service = BeanFactoryAnnotationUtils.qualifiedBeanOfType(
//    			applicationContext().getAutowireCapableBeanFactory(),
//    			CertificateProgramApplicationService.class,
//    			CaBridgeDomainServiceConfig.CERTIFICATE_PROGRAM_APP_SERVICE);
		// CONFIG2
//    	var service = applicationContext().getBean(
//    			CaBridgeDomainServiceConfig.CERTIFICATE_PROGRAM_APP_SERVICE_IMPL,
//    			CertificateProgramApplicationServiceCabImpl.class);
		// CONFIG3
//    	var service = applicationContext().getBean(
//    			CaBridgeDomainServiceConfig.CERTIFICATE_PROGRAM_APP_SERVICE,
//    			CertificateProgramApplicationService.class);
		// CONFIG4
//    	var service = applicationContext().getBean(CertificateProgramApplicationService.class);
		// CONFIG5
		var service = applicationContext().getBean(CertificateProgramApplicationServiceCabImpl.class);

//    	CertificateProgramApplicationService service = (CertificateProgramApplicationService) 
//    			applicationContext().getBean(CaBridgeDomainServiceConfig.CERTIFICATE_PROGRAM_APP_SERVICE);

		validateDataSourceIs(DataStore.ProgramDataStore, service.dataSource());
		return service;
	}


	public static ApplicationContext applicationContext() {
		if (applicationContext == null) {
			applicationContext = createApplicationContext();
		}
		return applicationContext;
	}

	private static void validateDataSourceIs(DataStore requiredSource, DataStore actualSource) {
		Assert.isTrue(requiredSource.equals(actualSource), "Data store must be " + requiredSource + " but is " + actualSource);
	}
}
